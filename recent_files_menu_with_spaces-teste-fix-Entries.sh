
#!/bin/bash
#Parse the file that stores the recent used files, send output to recent0.txt
awk -F"file://|\" " '/file:\/\// {print $2}' ~/.local/share/recently-used.xbel > /tmp/recent0.txt
#reverse contents order, so last file comes first, and so on...
tac /tmp/recent0.txt > /tmp/recent.txt
#process list of files, so they can be launched from jgmenu with the default launchers
while read p; do
urldecode() {
    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}
decoded=$(urldecode $p)
echo $decoded , xdg-open \"$decoded\"
#echo "$p" , xdg-open \"$decoded\"
done </tmp/recent.txt > /tmp/recent-jgmenu.txt
#Display menu with recent file
cat /tmp/recent-jgmenu.txt| jgmenu --simple

