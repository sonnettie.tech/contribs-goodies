��    
      l      �       �   @   �   U   2     �     �     �     �     �     �     �  �  �  F   �  �   �     S     b     i     l     t     �     �         
               	                      Incorrect time entered \n Enter a time between 00:00 and 23:59  Double click an event to edit or cancel it, double click date to add an all day event End (optional) Event OK Start \n  $event_description\n antiX Calendar antiX Calendar Alarm Project-Id-Version: contribs-goodies
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-01 11:01+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
  Hora inserida está incorreta \n Insira uma hora entre 00:00 e 23:59  Clique duas vezes em um evento para editá-lo ou cancelá-lo, clique duas vezes na data para adicionar um evento para o dia todo Fim (opcional) Evento OK Início \n $event_description\n Calendário do antiX Calendário com Alarme do antiX 