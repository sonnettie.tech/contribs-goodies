��    
      l      �       �   @   �   U   2     �     �     �     �     �     �     �     �  a       f      v     �     �     �     �     �  +            
               	                      Incorrect time entered \n Enter a time between 00:00 and 23:59  Double click an event to edit or cancel it, double click date to add an all day event End (optional) Event OK Start \n  $event_description\n antiX Calendar antiX Calendar Alarm Project-Id-Version: contribs-goodies
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-01 11:01+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Russian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ru/)
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Время недействительно. \n Действует время с 00:00 до 23:59.  Дважды щелкните левой кнопкой мыши на записи, чтобы отредактировать или удалить её.; Двойной щелчок по дате добавляет запись календаря на весь день. Конец (По желанию) Запись календаря Дальше Начало \n  $event_description\n antiX Календарь antiX Календарь будильник 