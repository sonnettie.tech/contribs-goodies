��    
      l      �       �   @   �   U   2     �     �     �     �     �     �     �  �  �  D   �  �   �     X  
   i     t     w     ~     �     �         
               	                      Incorrect time entered \n Enter a time between 00:00 and 23:59  Double click an event to edit or cancel it, double click date to add an all day event End (optional) Event OK Start \n  $event_description\n antiX Calendar antiX Calendar Alarm Project-Id-Version: contribs-goodies
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-01 11:01+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: French (Belgium) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr_BE/)
Language: fr_BE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
  Heure saisie erronée \n Saisissez une durée entre 00:00 et 23:59  Double clic sur un événement pour le modifier ou l'annuler, double clic sur la date afin d'ajouter un événement pour toute la journée Fin (facultatif) Evénement OK Début \n  $event_description\n antiX Calendrier antiX Calendrier Alarme 