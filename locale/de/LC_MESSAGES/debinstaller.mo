��          t      �                   (   5  =   ^  |   �  P     /   j  /   �  ;   �       �       �     �  (   �  B   �  �   9  U   �  F     &   `  b   �     �                   	                                  
       Debinstaller Debinstaller: $package Debinstaller: $package  $package_version Install !/usr/share/icons/papirus-antix/24x24/actions/add.png This option will completely UNINSTALL (PURGE) $package from your system.   \n Click OK if you are sure you want to continue  Uninstall !/usr/share/icons/papirus-antix/24x24/actions/xml-attribute-delete.png You are Root or running the script in sudo mode You entered the wrong password or you cancelled \n Finished!\n You can read the log or close this window.   already root Project-Id-Version: contribs-goodies
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-01 11:01+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Debinstaller Debinstaller: $package Debinstaller: $package  $package_version Installation !/usr/share/icons/papirus-antix/24x24/actions/add.png Diese Option wird $package deinstallieren und vollständig vom System entfernen („PURGE”). \n „Weiter” anklicken zum Fortsetzen.  Deinstallation !/usr/share/icons/papirus-antix/24x24/actions/xml-attribute-delete.png Das Script wurde von „Root” oder im „sudo”-Kontext aufgerufen. Falsches Paßwort oder Benutzerabbruch \n Fertig!\n Details können im Protokoll betrachtet und dieses Fenster kann geschlossen werden.   bereits „root“ 