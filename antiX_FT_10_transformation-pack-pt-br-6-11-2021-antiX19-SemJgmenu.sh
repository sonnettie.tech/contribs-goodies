#!/bin/bash
#
#13/2/2021- A versão 1.1 inclui as seguintes alterações: foram editados os ficheiros "keys" e "tint2rc" para não permitir que as janelas se sobreponham à barra do tint2 e, simultaneamente permitir utilização correcta da barra mesmo que sejam utilizados ícones no ambiente de trabalho (usando os ambientes "rox-fluxbox" ou o "space-fluxbox") ;  ícone do jector de dispositivos USB alterado, para se enquadrar melhor com os restantes ícones ; script executor de indicação interactiva de volume agora é criado, mas fica inativo (evita dois ícones de volume na barra, o fixo e o interactivo- isso foi um "bug" do instalador original do FT10). Para o activar, o utilizador deve renomear o ficheiro/arquivo "~/.config/tint2/vol-off" para "~/.config/tint2/vol" ; Para poupar recursos, o instalador já não procede à instalação automatica do "dunst" (quem desejar notificações de sistema, deverá instala-lo manualmente via Instalador de Pacotes/synaptic/terminal) ; Para ser mais simples utilizar lixeira/reciclagem no antiX, é instaldo o pacote "trash-cli" - mas o restante da configuração do space-fm para usar a reciclagem/lixeira, ainda tem que ser feita manualmente, utilizando as instruções disponíveis em https://www.antixforum.com/forums/topic/adicionar-reciclagem-ou-lixeira-ao-antix/
#
#To do: 1- criar script para exibir indicação de espaço disponível em disco na barra, que, tal como o indicador interactivo de volume, ficará inactivo por padrão, tendo que ser mudado o seu nome para surgir na barra; 2- configurar automaticamente a reciclagem/lixeira no Space-fm (deverá ser utilizado um ficheiro .desktop, colocado na barra do Space-fm para permitir localização do nome da reciclagem/lixeira em qualquer lingua contida no .desktop a parte mais trabalhosa é adicionar automaticamente o comando para utilizar o trash-cli associado à tecla "del" no spacefm)

configure ()
{
##################
#
#
#start tint2 and kill it, to generate default config file:
tint2 & sleep 2 &&  pkill tint2 &&
#Backup tint2 default config file:
cp ~/.config/tint2/tint2rc ~/.config/tint2/tint2rc.BAK &&
#
#
##################
# Create needed scripts:
#
cd ~/.config/tint2
# Create the scripts tint2 needs to work better
echo "=== 001. Create cpu"
sed 's/.//' >cpu <<'//GO.SYSIN DD script-01.sh'
 #!/bin/bash

 read cpu a b c previdle rest < /proc/stat
 prevtotal=$((a+b+c+previdle))
 sleep 1.5
 read cpu a b c idle rest < /proc/stat
 total=$((a+b+c+idle))
 cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
 mem=`free | awk '/Mem/ {printf "%d MB\n", $3 / 1024.0, $2 / 1024.0 }'`
 disk=$(df / |cut -d' ' -f12 |tail -1)
 #UnComment to show CPU icon (and if doing so, comment the "echo" line:
 #Please note an "echo" line here allows to be displayed, next to it, more than one line, piled horizontally
 #echo /usr/share/icons/papirus-antix/24x24/panel/indicator-sensors-cpu.png
  echo
 #UnComment to show CPU usage (in percetage):
  echo "CPU $cpu%"
 #UnComment to show available RAM:
 echo "RAM $mem"
 #UnComment to show available disk space in home:
 # echo " /     $disk"
 

//GO.SYSIN DD script-01.sh
chmod +x cpu

echo "=== 002. Create vol ('vol-off')"
sed 's/.//' >vol-off <<'//GO.SYSIN DD script-02.sh'
 #!/bin/bash
 vol=$(awk -F"[][]" '/dB/ { print $2 }' <(amixer sget Master)s)
 high=65
 medium=40
 low=0
	if [ "${vol::-1}" -ge 65 ]; then
		#echo /usr/share/icons/papirus-antix/48x48/actions/audio-volume-high.png
		echo /usr/share/icons/papirus-antix/24x24/panel/audio-volume-high.png
		echo "$vol"
	elif [ "${vol::-1}" -ge 40 ]; then
		#	echo /usr/share/icons/papirus-antix/48x48/actions/audio-volume-medium.png
			echo /usr/share/icons/papirus-antix/24x24/panel/audio-volume-medium.png
			echo "$vol"
	elif
	       [ "${vol::-1}" -ge 0 ]; then
       #			echo /usr/share/icons/papirus-antix/48x48/actions/audio-volume-low.png
	 	echo /usr/share/icons/papirus-antix/24x24/panel/audio-volume-low.png
	 		echo "$vol"	
	elif
	       [ "${vol::-1}" -eq 0 ]; then
       	#  			echo /usr/share/icons/papirus-antix/48x48/actions/audio-volume-muted.png
	 	echo /usr/share/icons/papirus-antix/24x24/panel/audio-volume-muted.png
	 		echo "$vol"	 			
 fi

//GO.SYSIN DD script-02.sh
chmod +x vol-off

echo "=== 003. Create auto-fluxbox-menu-position.sh"
sed 's/.//' >auto-fluxbox-menu-position.sh <<'//GO.SYSIN DD script-03.sh'
 #!/bin/bash
 #Get panel size from ~/.config/tint2/tint2rc
 panel_size=$(grep -hr "panel_size" ~/.config/tint2/tint2rc)
 toolbar_height=$(echo $panel_size | grep -o "%.*"|cut -c3-)
 toolbar_position=$(egrep "^panel_position =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
 toolbar_position_top_botom=$(echo $toolbar_position| cut -d' ' -f1)
 echo $toolbar_position_top_botom
 #select the right menu_height and the fluxbox menu will always come up right above the toolbar, just touching it, and not overlapting, adjusting to the panel's size

 if [ $toolbar_position_top_botom == "bottom" ] 
 then
 menu_height=540
 Yaxis=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2) && offset=$(($menu_height+$toolbar_height)) && correct_y_position_for_mouse_click=$(($Yaxis-$offset)) && eval $(xdotool getmouselocation --shell) && xdotool mousemove 0 $correct_y_position_for_mouse_click && fluxbox-remote rootmenu && xdotool mousemove $X $Y
 fi

 if [ $toolbar_position_top_botom == "top" ] 
 then
 Yaxis=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2) 
 menu_height=540 
 offset=$(($menu_height+$toolbar_height)) 
 correct_y_position_for_mouse_click=$((6+$toolbar_height)) 
 eval $(xdotool getmouselocation --shell) 
 xdotool mousemove 0 $correct_y_position_for_mouse_click && fluxbox-remote rootmenu && xdotool mousemove $X $Y
 fi

 if [ $toolbar_position_top_botom == "left" ] 
 then
 fluxbox-remote rootmenu
 fi

 if [ $toolbar_position_top_botom == "right" ] 
  then
   fluxbox-remote rootmenu
 fi

//GO.SYSIN DD script-03.sh
chmod +x auto-fluxbox-menu-position.sh

echo "=== 004. Create tim-tint-pt.sh"
sed 's/.//' >tim-tint-pt.sh <<'//GO.SYSIN DD script-04.sh'
 #!/bin/bash
 # Adds/Removes icons to the Tint2 toolbar(adding icons is done using the info from the app's .desktop file) 
 # By PPC, 6/01/2021 adapted from many, many on-line examples, and from toolbar icon manager for icewm
 # GPL licence - feel free to improve/adapt this script - but keep the lines about the license and author

 ###Check if tint2 is running, if not, exit
 running_windows=$(wmctrl -l)
 if [[ $running_windows == *" tint2"* ]]; then
  echo "You are running tint2..."
    else 
     yad --window-icon=/usr/share/pixmaps/icewm_editor.png --title='Aviso' --text='This script is meant to be run only with tint2 toolbar!!!' --timeout=10 --no-buttons --center
     exit
 fi

 ### show "please wait"
 yad --title="tint2-wait" --center --undecorated --no-buttons --text="\n  Por favor aguarde... " &

 ###Create list of availables apps, with localized names- this takes a few seconds on low powered devices:

 #Get system language (to allow localization):
 lang=$(locale | grep LANG | cut -d= -f2 | cut -d_ -f1) #To test localization to another language, like french, use: lang=fr
 window_icon="/usr/share/icons/papirus-antix/24x24/apps/tint2.png"
 #Loop through all .desktop files in the applications folders and extract name and save that to a .txt file 

 cd /usr/share/applications/
 find ~+ -type f -name "*.desktop" > ~/.apps-antix-0.txt
 ###NOTE: repeat the last 2 lines, changing the first, the "cd" one, to change to the folder where your .desktop files are
 sort ~/.apps-antix-0.txt > ~/.apps-antix.txt
 cd ~

 for file in $(cat ~/.apps-antix.txt)
 do
   name1=$(grep -o -m 1 '^Name=.*' $file)

   ### localized menu entries generator (slows the script down, but produces nearly perfectly localized menus):
    name2=$name1
	translated_name1=$(grep -o -m 1 "^Name\[$lang\]=.*" $file)
	[ -z "$translated_name1" ] && note="No localized name found, using the original one" || name2=$translated_name1
	#if the desktop file has the string "Desktop Action" simply use the original untranslated name, to avoid risking using a translation that's not the name of the app
	grep -q "Desktop Action" $file && name2=$name1
	name1=$name2
   ### end of localized menu entries generator	 
   name=$(echo $name1|sed 's/.*\=//') 

 echo "$name"  --  "$file"
 done > /tmp/list.txt
 sort /tmp/list.txt > ~/.apps.txt
 ###

 delete_icon()
 {
 for (( ; ; ))
 do
	
 ###Function to delete  icon
 #criar lista de ícones:
 grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
 #remover o texto "launcher_item_app = ", 21 leters
 (cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt

 #apagar ficheiro temporiario
 echo > /tmp/lista_apelativa.txt
 #processar linha a linha a lista dos launchers existentes na barra
 cat /tmp/launchers.txt | while read line; do
 ### ver se o ficheiros apps.txt, que tem todos os programas, com descrição, tem uma linha que corresponde à aplicação que está a ser analisada, a "line" da lista launchers.txt
 app_with_descri=$(grep -E $line ~/.apps.txt)
 #se não houver correspondencia, manter o nome do .desktop
 [ -z "$app_with_descri" ] && app_with_descri=$line
 echo $app_with_descri >> /tmp/lista_apelativa.txt
 done

 #choose application to delete
		EXEC=$(yad   --window-icon=window_icon --title="Escolha o ícone a ser removido da barra"  --height=550 --width=650 --center  --separator=" " --list  --column= --button=" !/usr/share/icons/papirus-antix/24x24/actions/xml-attribute-delete.png" < /tmp/lista_apelativa.txt )
 #choose only path and desktop file (last field from string:
 selection_to_delete=$(echo $EXEC | awk '{print $NF}')

 # if no application selected- avoids creating empty icon: (also allows to exit the infinit loop tha keeps respawning the selection window)
 if [ -z "$EXEC" ]; then exit
 fi

 #delete application icon from toolbar
 grep -v $selection_to_delete ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 

 #instantly restart toolbar to show changes
 old_tint2_pid=$(pgrep tint2)
 nohup  tint2 && sleep 0.1 &&
 kill -9  $old_tint2_pid &

 #refresh list of launchers
 echo > /tmp/lista_apelativa.txt
 cat /tmp/launchers.txt | while read line; do
 app_with_descri=$(grep -E $line ~/.apps.txt)
 [ -z "$app_with_descri" ] && app_with_descri=$line
 echo $app_with_descri >> /tmp/lista_apelativa.txt
 done

 #end initial infine loop (so window is always open after selection)
 done
 }		

 move_icon()
 {
	#criar lista de ícones:
 grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
 #remover o texto "launcher_item_app = ", 21 leters
 (cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt

 # estabelecer linha que é o limite minimo esquerdo até onde um ícone pode ser movido
 limite_esq_zona=$(grep -n -m 1 "# Launcher" ~/.config/tint2/tint2rc |sed  's/\([0-9]*\).*/\1/')
 limite_esq_launchers=$(grep -n -m 1 "launcher_item_app =" ~/.config/tint2/tint2rc |sed  's/\([0-9]*\).*/\1/')
 if [ $limite_esq_launchers -ge $limite_esq_zona ]; then limite_esq=$limite_esq_launchers
  else
  limite_esq=$limite_esq_zona
 fi
  limite_esq_final=$(($limite_esq -1 ))
  echo $limite_esq_final

 # estabelecer linha que é o limite máximo direito até onde um ícone pode ser movido 
 #number_of_lines=$(wc -l < ~/.config/tint2/tint2rc)
 limite_dir_launchers=$(grep -n launcher_item_app ~/.config/tint2/tint2rc |cut -d':' -f1 |tail -n 1)
 
 #apagar ficheiro temporiario
 echo > /tmp/lista_apelativa.txt
 #processar linha alinha a lista dos launchers existentes na barra
 cat /tmp/launchers.txt | while read line; do
 ### ver se o ficheiros apps.txt, que tem todos os programas, com descrição, tem uma linha que corresponde à aplicação que está a ser analisada, a "line" da lista launchers.txt
 app_with_descri=$(grep -E $line ~/.apps.txt)
 #se não houver correspondencia, manter o nome do .desktop
 [ -z "$app_with_descri" ] && app_with_descri=$line
 echo $app_with_descri >> /tmp/lista_apelativa.txt
 done

 #choose application to move
		EXEC=$(yad --window-icon=$window_icon --title="Escolha o ícone que deseja mover na barra" --width=450 --height=480 --center --separator=" " --list  --column=" "  < /tmp/lista_apelativa.txt --button=' !/usr/share/icons/papirus-antix/24x24/actions/kt-upnp.png':4)

		#get line number(s) where the choosen application is
		x=$(echo $EXEC)
		#get last filed of the selected line in practice it's the path and file name of the .desktop file)
		x=$(echo $EXEC | awk '{print $NF}')
		Line=$(grep -n -m 1 $x ~/.config/tint2/tint2rc |cut -f1 -d: )

		 #get number of lines in file
		 number_of_lines=$(wc -l < $file)
		 
 #####Act on selection:
 #only do something if a icon was selected :
  if test -z "$x" 
  then
      echo "nothing was selected"
  else

   file_name=~/.config/tint2/tint2rc
   a=$Line

 #this performs an infinite loop, so the Move window is ALWAYS open unless the user clicks "Cancel"
	while :
	do

  nome=$(echo $EXEC |cut -d'-' -f1)
  yad --window-icon=/usr/share/pixmaps/icewm_editor.png --center --undecorated --title="Mover ícone da barra" --text="A mover o ícone $nome" \
 --button=" !/usr/share/icons/papirus-antix/24x24/actions/gtk-quit.png":1 \
 --button=" ← ":2 \
 --button=" → ":3 

 foo=$?
 Line_to_the_left=line_number=$((line_number-1))
 line_number=$a

 if [[ $foo -eq 1 ]]; then exit
 fi

 #move icon to the left:
 limite_esquerdo=0
 if [[ $foo -eq 2 ]]; then
 b=$(($a-1))
	if [ $b -gt $limite_esq_final ]; then
 sed -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} > test2.txt
 #create backup file before changes
 cp ~/.config/tint2/tint2rc ~/.config/tint2/tint2rc.bak
 cp test2.txt ~/.config/tint2/tint2rc
 sleep .3

 old_tint2_pid=$(pgrep tint2)
 nohup  tint2 && sleep 0.1 &&
 kill -9  $old_tint2_pid &

 a=$(($a-1))   # update selected icon's position, just in case the user wants to move it again
	fi
 fi

 #move icon to the right:
 if [[ $foo -eq 3 ]]; then
 a=$(($a+1))
 b=$(($a-1))
    if [ $b -ge  $limite_dir_launchers ]; then 
      exit 
  else
  sed -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} > test2.txt
 #create backup file before changes 
 cp ~/.config/tint2/tint2rc ~/.config/tint2/tint2rc.bak
    cp test2.txt  ~/.config/tint2/tint2rc
    sleep .3

 old_tint2_pid=$(pgrep tint2)
 nohup  tint2 && sleep 0.1 &&
 kill -9  $old_tint2_pid &
 # There's no need to update selected icon's position, just in case the user wants to move it again, because moving right just moves the icon to the right of the select icon to the left, so, it updates instantly the selected icon's position
  fi
 fi

	done

 fi ### ends if cicle that checks if user selected icon to move in the main Move icon window

	}	

 add_icon()
 {

 ####begin infinite loop
 for (( ; ; ))
 do

 # Use a Yad window to select file to be added to the menu
 DADOS=$(yad --window-icon=$window_icon --title="Escolha o ícone a adicionar à barra"  --height=550 --width=650 --center  --separator=" " --list  --column= --button=" !/usr/share/icons/papirus-antix/24x24/actions/add.png" < ~/.apps.txt)
 SELECTION=$(echo $DADOS | sed 's/^.*--//')
		###Function to add a new icon
 COMANDO0=$(echo "$DADOS" | cut -d'|' -f1)
 #this strips any existing path from the name:
 EXEC0=$(grep Exec= $COMANDO0 | cut -d '=' -f2)
 EXEC=$(echo "$EXEC0" | cut -f1 -d" ")
    
 # if no application selected- avoids creating empty icon: (also allows to exit the infinit loop tha keeps respawning the selection window)
 if [ -z "$EXEC" ]; then exit
 fi

 #add line to toolbar
 echo "launcher_item_app ="$SELECTION  >> ~/.config/tint2/tint2rc
 #instantly restart toolbar to show changes
 old_tint2_pid=$(pgrep tint2)
 nohup  tint2 && sleep 0.1 &&
 kill -9  $old_tint2_pid &


 #end initial infine loop (so window is always open after selection)
 done
		###END of Function to add a new icon
 }

 ##check if window with the title "tint2-wait" is showing, if so, close that window:
 wmctrl -lp | awk '/tint2-wait/{print $3}' | xargs kill

 #display main window
 export -f delete_icon add_icon move_icon
 DADOS=$(yad --window-icon=$window_icon --paned --center --splitter="200" --title="Gerir ícones da barra" \
 --form  \
 --center \
 --button=" !/usr/share/icons/papirus-antix/24x24/actions/xml-attribute-delete.png":"bash -c delete_icon" \
 --button=" !/usr/share/icons/papirus-antix/24x24/actions/kt-upnp.png":"bash -c move_icon" \
 --button=" !/usr/share/icons/papirus-antix/24x24/actions/add.png":"bash -c add_icon" \
 --wrap)

 ### wait for a button to be pressed then perform the selected function
 foo=$?

 [[ $foo -eq 1 ]] && exit 0

//GO.SYSIN DD script-04.sh
chmod +x tim-tint-pt.sh

# Perform conf file changes
echo "###Tint2 ###"
echo "session.screen0.allowRemoteActions: true" >> ~/.fluxbox/init
echo "session.screen0.toolbar.tools: rootmenu, iconbar, clock" >> ~/.fluxbox/init
echo "session.screen0.toolbar.visible: false" >> ~/.fluxbox/init
#this one is optinal, changes to "Numix" theme, with a more modern, dark look:
echo "session.styleFile: /usr/share/fluxbox/styles/Numix">> ~/.fluxbox/init
#
cd ~/.fluxbox
> apps
echo "[app] (name=tint2)" >> ~/.fluxbox/apps
echo "[Layer] {6}" >> ~/.fluxbox/apps
echo "[end]" >> ~/.fluxbox/apps
#
sed '2a\
~/.fehbg & \
tint2 &
' < ~/.fluxbox/startup >/tmp/t2instal
cp /tmp/t2instal ~/.fluxbox/startup &&
#make spacefm the default file manager:
sudo cp  /usr/share/applications/spacefm.desktop ~/.local/share/desktop-defaults/file-manager.desktop
#
# Generate tint2 config file:
cd ~/.config/tint2
echo "=== Create tint2rc"
sed 's/.//' >tint2rc <<'//GO.SYSIN DD script-00.sh'
 #---- Generated by tint2conf 09a9 ----
 # See https://gitlab.com/o9000/tint2/wikis/Configure for 
 # full documentation of the configuration options.
 #-------------------------------------
 # Gradients
 # Gradient 1
 gradient = vertical
 start_color = #2d2d2d 80
 end_color = #000000 80
 color_stop = 70.000000 #1c1c1c 80
 
 #-------------------------------------
 # Backgrounds
 # Background 1: Clock, Panel, Tooltip
 rounded = 0
 border_width = 1
 border_sides = T
 border_content_tint_weight = 0
 background_content_tint_weight = 0
 background_color = #000000 80
 border_color = #333333 80
 gradient_id = 1
 background_color_hover = #000000 80
 border_color_hover = #555555 80
 background_color_pressed = #000000 80
 border_color_pressed = #555555 80
 
 # Background 2: Default task, Iconified task
 rounded = 0
 border_width = 2
 border_sides = B
 border_content_tint_weight = 100
 background_content_tint_weight = 25
 background_color = #777777 0
 border_color = #777777 100
 background_color_hover = #464646 100
 border_color_hover = #cccccc 30
 background_color_pressed = #1e1e1e 100
 border_color_pressed = #777777 30
 
 # Background 3: Active task
 rounded = 0
 border_width = 2
 border_sides = B
 border_content_tint_weight = 100
 background_content_tint_weight = 100
 background_color = #ffffff 100
 border_color = #d9d9d9 100
 background_color_hover = #ffffff 73
 border_color_hover = #d9d9d9 100
 background_color_pressed = #989898 73
 border_color_pressed = #d9d9d9 100
 
 # Background 4: Urgent task
 rounded = 0
 border_width = 0
 border_sides = TBLR
 border_content_tint_weight = 0
 background_content_tint_weight = 0
 background_color = #aa4400 100
 border_color = #aa7733 100
 background_color_hover = #aa4400 100
 border_color_hover = #aa7733 100
 background_color_pressed = #aa4400 100
 border_color_pressed = #aa7733 100
 
 # Background 5: 
 rounded = 2
 border_width = 1
 border_sides = TBLR
 border_content_tint_weight = 0
 background_content_tint_weight = 0
 background_color = #ffffaa 100
 border_color = #999999 100
 background_color_hover = #ffffaa 100
 border_color_hover = #999999 100
 background_color_pressed = #ffffaa 100
 border_color_pressed = #999999 100
 
 # Background 6: Inactive desktop name
 rounded = 0
 border_width = 2
 border_sides = B
 border_content_tint_weight = 0
 background_content_tint_weight = 0
 background_color = #777777 0
 border_color = #777777 0
 background_color_hover = #bdbdbd 21
 border_color_hover = #cccccc 100
 background_color_pressed = #777777 21
 border_color_pressed = #777777 100
 
 # Background 7: Active desktop name
 rounded = 0
 border_width = 2
 border_sides = B
 border_content_tint_weight = 0
 background_content_tint_weight = 0
 background_color = #ffffff 21
 border_color = #d9d9d9 100
 background_color_hover = #ffffff 21
 border_color_hover = #d9d9d9 100
 background_color_pressed = #a9a9a9 21
 border_color_pressed = #d9d9d9 100
 
 #-------------------------------------
 # Panel
 panel_items = PPPPPLTEEEEPPPPSBCPP
 panel_size = 100% 30
 panel_margin = 0 0
 panel_padding = 0 0 2
 panel_background_id = 1
 wm_menu = 0
 panel_dock = 0
 panel_pivot_struts = 0
 panel_position = bottom left horizontal
 panel_layer = normal
 panel_monitor = all
 panel_shrink = 0
 autohide = 0
 autohide_show_timeout = 0
 autohide_hide_timeout = 0.5
 autohide_height = 2
 strut_policy = follow_size
 panel_window_name = tint2
 disable_transparency = 0
 mouse_effects = 1
 font_shadow = 0
 mouse_hover_icon_asb = 100 0 10
 mouse_pressed_icon_asb = 100 0 0
 scale_relative_to_dpi = 0
 scale_relative_to_screen_height = 0
 
 #-------------------------------------
 # Taskbar
 taskbar_mode = single_desktop
 taskbar_hide_if_empty = 0
 taskbar_padding = 0 0 0
 taskbar_background_id = 0
 taskbar_active_background_id = 0
 taskbar_name = 0
 taskbar_hide_inactive_tasks = 0
 taskbar_hide_different_monitor = 0
 taskbar_hide_different_desktop = 0
 taskbar_always_show_all_desktop_tasks = 0
 taskbar_name_padding = 4 0
 taskbar_name_background_id = 6
 taskbar_name_active_background_id = 7
 taskbar_name_font = sans bold 9
 taskbar_name_font_color = #dddddd 100
 taskbar_name_active_font_color = #dddddd 100
 taskbar_distribute_size = 1
 taskbar_sort_order = none
 task_align = left
 
 #-------------------------------------
 # Task
 task_text = 0
 task_icon = 1
 task_centered = 1
 urgent_nb_of_blink = 100000
 task_maximum_size = 45 30
 task_padding = 2 3 1
 task_font = Sans 8
 task_tooltip = 1
 task_thumbnail = 1
 task_thumbnail_size = 210
 task_font_color = #c6c6c6 100
 task_active_font_color = #ffffff 100
 task_icon_asb = 100 0 0
 task_background_id = 2
 task_active_background_id = 3
 task_urgent_background_id = 4
 task_iconified_background_id = 2
 mouse_left = toggle_iconify
 mouse_middle = maximize_restore
 mouse_right = close
 mouse_scroll_up = prev_task
 mouse_scroll_down = next_task
 
 #-------------------------------------
 # System tray (notification area)
 systray_padding = 0 0 2
 systray_background_id = 0
 systray_sort = ascending
 systray_icon_size = 22
 systray_icon_asb = 100 0 0
 systray_monitor = 1
 systray_name_filter = 
 
 #-------------------------------------
 # Launcher
 launcher_padding = 0 0 0
 launcher_background_id = 0
 launcher_icon_background_id = 0
 launcher_icon_size = 0
 launcher_icon_asb = 100 0 0
 launcher_icon_theme = papirus-antix
 launcher_icon_theme_override = 0
 startup_notifications = 0
 launcher_tooltip = 1
 launcher_item_app = /usr/share/applications/firefox-esr.desktop
 launcher_item_app = /usr/share/applications/libreoffice-writer.desktop
 launcher_item_app = /usr/share/applications/packageinstaller.desktop
 launcher_item_app = /usr/share/applications/antix/updater.desktop

 #-------------------------------------
 # Clock
 time1_format = %H:%M
 time2_format = %a %-d %b
 time1_font = Iosevka Bold 8
 time1_timezone = 
 time2_timezone = 
 time2_font = Iosevka 8
 clock_font_color = #e1e1e1 100
 clock_padding = 0 0
 clock_background_id = 1
 clock_tooltip = 
 clock_tooltip_timezone = 
 clock_lclick_command = yad --calendar --on-top --undecorated --mouse --details=$HOME/yad-calendar.txt --button="X":1
 clock_rclick_command = 
 clock_mclick_command = 
 clock_uwheel_command = 
 clock_dwheel_command = 
 
 #-------------------------------------
 # Battery
 battery_tooltip = 1
 battery_low_status = 10
 battery_low_cmd = 
 battery_full_cmd = 
 bat1_font = sans 8
 bat2_font = sans 6
 battery_font_color = #eeeeee 100
 bat1_format = 
 bat2_format = 
 battery_padding = 1 0
 battery_background_id = 0
 battery_hide = 101
 battery_lclick_command = 
 battery_rclick_command = 
 battery_mclick_command = 
 battery_uwheel_command = 
 battery_dwheel_command = 
 ac_connected_cmd = 
 ac_disconnected_cmd = 
 
 #-------------------------------------
 # Executor 1
 execp = new
 execp_command = ~/.config/tint2/cpu
 execp_interval = 2
 execp_has_icon = 1
 execp_cache_icon = 1
 execp_continuous = 0
 execp_markup = 1
 execp_tooltip = CPU
 execp_lclick_command = lxtask
 execp_rclick_command = 
 execp_mclick_command = 
 execp_uwheel_command = 
 execp_dwheel_command = 
 execp_font = Sans 6
 execp_font_color = #ffffff 100
 execp_padding = 0 0
 execp_background_id = 2
 execp_centered = 0
 execp_icon_w = 24
 execp_icon_h = 24
 
 #-------------------------------------
 # Executor 2
 execp = new
 execp_command = ~/.config/tint2/mem
 execp_interval = 3
 execp_has_icon = 1
 execp_cache_icon = 1
 execp_continuous = 0
 execp_markup = 1
 execp_tooltip = RAM
 execp_lclick_command = lxtask
 execp_rclick_command = 
 execp_mclick_command = 
 execp_uwheel_command = 
 execp_dwheel_command = 
 execp_font = Sans 6
 execp_font_color = #ffffff 100
 execp_padding = 0 0
 execp_background_id = 2
 execp_centered = 0
 execp_icon_w = 24
 execp_icon_h = 24
 
 #-------------------------------------
 # Executor 3
 execp = new
 execp_command = ~/.config/tint2/vol
 execp_interval = 0
 execp_has_icon = 1
 execp_cache_icon = 1
 execp_continuous = 0
 execp_markup = 1
 execp_tooltip = Volume
 execp_lclick_command = yad-volume
 execp_rclick_command = pkill yad
 execp_mclick_command = 
 execp_uwheel_command = amixer sset Master,0 1+
 execp_dwheel_command = amixer sset Master,0 1-
 execp_font = Sans 6
 execp_font_color = #f8f3c7 100
 execp_padding = 0 0
 execp_background_id = 2
 execp_centered = 0
 execp_icon_w = 0
 execp_icon_h = 0
 
 #-------------------------------------
 # Executor 4
 execp = new
 execp_command = ~/.config/tint2/net
 execp_interval = 2
 execp_has_icon = 1
 execp_cache_icon = 1
 execp_continuous = 0
 execp_markup = 1
 execp_tooltip = Internet
 execp_lclick_command = cmst -d
 execp_rclick_command = 
 execp_mclick_command = 
 execp_uwheel_command = 
 execp_dwheel_command = 
 execp_font = Sans 6
 execp_font_color = #f8f3c7 100
 execp_padding = 0 0
 execp_background_id = 2
 execp_centered = 0
 execp_icon_w = 0
 execp_icon_h = 0
 
 #-------------------------------------
 # Executor 5
 execp = new
 execp_command = 
 execp_interval = 0
 execp_has_icon = 0
 execp_cache_icon = 1
 execp_continuous = 0
 execp_markup = 1
 execp_lclick_command = 
 execp_rclick_command = 
 execp_mclick_command = 
 execp_uwheel_command = 
 execp_dwheel_command = 
 execp_font_color = #000000 100
 execp_padding = 0 0
 execp_background_id = 0
 execp_centered = 0
 execp_icon_w = 0
 execp_icon_h = 0
 
 #-------------------------------------
 # Button 1
 button = new
 button_text = 
 button_tooltip = Menu Iniciar
 button_lclick_command = fluxbox-remote rootmenu
 button_rclick_command = ~/.config/tint2/auto-fluxbox-menu-position.sh
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font = Sans 24
 button_font_color = #e6e9ef 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 2
 button = new
 button_icon = /usr/share/icons/papirus-antix/48x48/actions/find.png
 button_text = 
 button_tooltip = Procurar aplicativos / Procurar arquivos
 button_lclick_command = rofi -modi window -show drun -show-icons -sidebar-mode  -theme /usr/share/rofi/themes/fancy.rasi
 button_rclick_command = xdg-open "$(locate home media | rofi -theme /usr/share/rofi/themes/fancy.rasi -threads 0 -dmenu -i -p "locate:")"
 button_mclick_command = app-select
 button_uwheel_command = 
 button_dwheel_command = 
 button_font = Sans 24
 button_font_color = #cbcbcb 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 3
 button = new
 button_icon = /usr/share/icons/papirus-antix/48x48/actions/view-multiple-objects.png
 button_text = 
 button_tooltip = Alternar Janelas / Exibir fundo do ambiente de trabalho 
 button_lclick_command = skippy-xd
 button_rclick_command = currentwindowid=$(xdotool getactivewindow) && for w in $(xdotool search --all --maxdepth 3 --name ".*"); do   if [ $w -ne $currentwindowid ] ; then     xdotool windowminimize "$w";   fi; done && pkill tint2 && nohup tint2 &
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 0 0
 button_background_id = 1
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 4
 button = new
 button_text = +/ -
 button_tooltip = Gerir ícones da barra (adicionar/remover/mover ícones)
 button_lclick_command = ~/.config/tint2/tim-tint-pt.sh
 button_rclick_command = 
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #5c616c 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 5
 button = new
 button_icon = /usr/share/icons/papirus-antix/48x48/places/user-home.png
 button_text = 
 button_tooltip = Gerenciador de Arquivos
 button_lclick_command = spacefm
 button_rclick_command = 
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 6
 button = new
 button_icon = /usr/share/icons/papirus-antix/24x24/panel/indicator-rss-aware.png
 button_text = 
 button_tooltip = Gerenciador de Redes - Connman
 button_lclick_command = WINID=$(wmctrl -lx | grep 'Connman System Tray' | awk 'NR==1{print $1}'); if [ $WINID ]; then  wmctrl -ia $WINID & else    cmst -d & fi
 button_rclick_command = 
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 7
 button = new
 button_icon = /usr/share/icons/papirus-antix/24x24/panel/audio-volume-high.png
 button_text = 
 button_tooltip = Volume
 button_lclick_command = screenX=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1) ; screenY=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2) ; x=`expr $screenX - 310` ; y=`expr $screenY - 60` ; yad --scale --max-value 100 --value $(amixer -M get Master | awk -v FS="[[%]" '/%/ {print $2}') --print-partial --undecorated --width 300 --fixed --sticky --posx=$x --posy=$y --on-top --escape-ok --no-buttons --close-on-unfocus   | while read VolNew; do amixer -M -q set Master ${VolNew}%; done
 button_rclick_command = pkill yad
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 8
 button = new
 button_icon = /usr/share/icons/papirus-antix/24x24/panel/weather-few-clouds.png
 button_text = 
 button_tooltip = Previsão do Tempo
 button_lclick_command = WINID=$(wmctrl -lx | grep 'Simple Weather' | awk 'NR==1{print $1}'); if [ $WINID ]; then  wmctrl -ia $WINID & else roxterm --hide-menubar --title "Simple Weather" --zoom=0,85 --geometry 130x40 -e bash -c "curl 'http://wttr.in/?lang=pt'; read -N1;" & fi
 button_rclick_command = 
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 9
 button = new
 button_icon = /usr/share/icons/papirus-antix/48x48/actions/media-eject.png
 button_text = 
 button_tooltip = Ejetar dispostivos USB
 button_lclick_command = gksu unplugdrive.sh
 button_rclick_command = 
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 1 1
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 10
 button = new
 button_icon = /usr/share/icons/antix-papirus/application-exit.png
 button_text = 
 button_tooltip = Sair
 button_lclick_command = WINID=$(wmctrl -lx | grep 'Exit Session' | awk 'NR==1{print $1}'); if [ $WINID ]; then  wmctrl -ia $WINID & else    desktop-session-exit & fi
 button_rclick_command = 
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Button 11
 button = new
 button_text = 
 button_lclick_command = 
 button_rclick_command = 
 button_mclick_command = 
 button_uwheel_command = 
 button_dwheel_command = 
 button_font_color = #000000 100
 button_padding = 0 0
 button_background_id = 0
 button_centered = 0
 button_max_icon_size = 0
 
 #-------------------------------------
 # Tooltip
 tooltip_show_timeout = 0
 tooltip_hide_timeout = 0
 tooltip_padding = 8 4
 tooltip_background_id = 1
 tooltip_font_color = #66653d 100
 tooltip_font = Noto Sans 10
 
//GO.SYSIN DD script-00.sh

}

export -f install configure
roxterm --hide-menubar --title "Installing antiX FT 10 Transformation Pack..." -e bash -c "gksu apt update && sudo apt install -y tint2 rofi skippy-xd compton trash-cli;  pkill tint2 ; sleep 2 && bash -c configure"
#removed, from the previous line, before the double quote:
#; nohup tint2

#change to min-fluxbox desktop
/usr/local/lib/desktop-session/desktop-session-restart min-fluxbox 
#
exit
